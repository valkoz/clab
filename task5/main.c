#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/wait.h>
#include <string.h>
#include <errno.h>
#include <sys/ipc.h>
#include <sys/msg.h>
// Add queue with task1 -> ipcs -> ./main {ipcs[0].id}
int main (int argc, char* argv[]) {
    int msgid = atoi(argv[1]);

    if (msgctl(msgid, IPC_RMID, NULL) == -1) {
        fprintf(stderr, "Message queue could not be deleted.\n");
        exit(EXIT_FAILURE);
    }

    printf("Message queue was deleted.\n");

    return 0;
}
