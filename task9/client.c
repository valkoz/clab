#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/wait.h>
#include <string.h>
#include <errno.h>
#include <sys/msg.h>

#define MSG_SIZE 64
//run server ./server
//run clients: ./client 1 ./client 1 ./client 2 ./client 2
// ./client 1 : argv[1] receive msg type
typedef struct my_msgbuf {
    long mtype;
    int msg_qid;
    char payload[MSG_SIZE]; 
} my_msgbuf;

int send_message(int qid, my_msgbuf *qbuf)
{
    int res;
    int length;
    length = sizeof(my_msgbuf) - sizeof(long);        
    res = msgsnd(qid, qbuf, length, 0);
    if (res == -1) {
        //perror("Msgsnd: ");
        return 1;
    }
    return 0;
}

int read_message(int qid, long type, my_msgbuf *qbuf)
{
    int res;
    int length;
    length = sizeof(my_msgbuf) - sizeof(long);   
    res = msgrcv(qid, qbuf, length, type, 0);
    if (res == -1) {
        //perror("Msgrcv: ");
        return 1;
    }
    return 0;
}

int main (int argc, char* argv[]) {
    int key = ftok("./server", 'I');
    if (key == -1) {
        perror("Ftok: ");
        return 1;
    }
    int server_qid = msgget(key, 0666);
    if (server_qid == -1) {
        perror("Msgget: ");
        return 1;
    }

    key = ftok("./client", 1);
    if (key == -1) {
        perror("Ftok: ");
        return 1;
    }
    int client_qid = msgget(key, 0666 | IPC_CREAT);
    if (client_qid == -1) {
        perror("Msgget: ");
        return 1;
    }

    printf("Client has started\n");

    struct my_msgbuf r_msg;
    struct my_msgbuf s_msg;
    char buffer[MSG_SIZE];
    int flag = 1;
    int res;

    int pid;
    pid = fork();
    if (pid == 0) {
        s_msg.mtype = atoi(argv[1]);
        s_msg.msg_qid = client_qid;
        strcpy(s_msg.payload, "ping\n");
        res = send_message(server_qid, &s_msg);
        if (res == 1) {
            flag = 0;
        }
        while(flag == 1) {
            memset(buffer, 0, sizeof buffer);
            read(0, buffer, MSG_SIZE);
            s_msg.mtype = atoi(argv[1]);
            s_msg.msg_qid = client_qid;
            strcpy(s_msg.payload, buffer);
            res = send_message(server_qid, &s_msg);
            if (res == 1) {
                flag = 0;
            }
        }

        if (msgctl(client_qid, IPC_RMID, NULL) == -1) {
            fprintf(stderr, "Message queue could not be deleted.\n");
            exit(EXIT_FAILURE);
        }

        printf("Sender Die.\n");
        return 0;

    }
    else {
        while(flag == 1) {
            res = read_message(client_qid, atoi(argv[1]), &r_msg);
            if (res == 1) {
                flag = 0;
            }
            printf("Response from server: %s", r_msg.payload);
        }

        printf("Receiver die.\n");
        return 0;
    }
}
