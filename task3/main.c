#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/wait.h>
#include <string.h>
#include <errno.h>
#include <sys/msg.h>
// task1 send message, this receive them 
// takes last 5 messages of given type (don't wait) 
// ./main {msg.id} {msg.type}
#define MY_DATA 22
typedef struct my_msgbuf {
    long mtype;
    char payload[MY_DATA]; 
} my_msgbuf;

int read_message(int qid, long type, my_msgbuf *qbuf)
{
    int res;
    res = msgrcv(qid, qbuf, MY_DATA, type, IPC_NOWAIT);
    if (res == -1) {
        perror("Msgrcv: ");
        return 1;
    }
    return 0;
}

int main (int argc, char* argv[]) {
    int msqid = atoi(argv[1]);
    int mtype = atoi(argv[2]);
    int res;

    for(int i = 0; i < 5; i++) {
        struct my_msgbuf msg;
        res = read_message(msqid, mtype, &msg);
        if (res == 0) {
            printf("Received: %s\n", msg.payload);
        }
    }
}
