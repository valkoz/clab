#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/wait.h>
#include <string.h>
#include <errno.h>
#include <sys/msg.h>
// delete queue: ipcrm msg 131072
// show queues: ipcs
#define MY_DATA 22
typedef struct my_msgbuf {
    long mtype;
    char payload[MY_DATA]; 
} my_msgbuf;

int send_message(int qid, my_msgbuf *qbuf)
{
    int res;
    res = msgsnd(qid, qbuf, MY_DATA, 0);
    if (res == -1) {
        perror("Msgsnd: ");
        return 1;
    }
}

int main (int argc, char* argv[]) {
    int key = ftok("./main", 'I');
    if (key == -1) {
        perror("Ftok: ");
        return 1;
    }
    int msqid = msgget(key, 0666 | IPC_CREAT);
    if (msqid == -1) {
        perror("Msgget: ");
        return 1;
    }

    struct msqid_ds ds;

    int res = msgctl(msqid, IPC_STAT, &ds);
    if (res == -1) {
        perror("Msgctl: ");
        return 1;
    }

    printf("Owner's UID: %d\n", ds.msg_perm.uid);
    printf("Owner's GID: %d\n", ds.msg_perm.gid);
    printf("Message stime: %ld\n", ds.msg_stime);
    printf("Message rtime: %ld\n", ds.msg_rtime);
    printf("Message ctime: %ld\n", ds.msg_ctime);
    printf("Number of messages: %ld\n", ds.msg_qnum);
    printf("Maximum number of bytes: %ld\n", ds.msg_qbytes);


    for(int i = 1; i < 5; i++) {
        struct my_msgbuf msg;
        msg.mtype = i;
        strcpy(msg.payload, "Very important message");
        send_message(msqid, &msg);
    }
}
