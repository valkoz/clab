#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/wait.h>
#include <string.h>
#include <errno.h>
#include <sys/msg.h>
#include <stdbool.h>
// ./server
#define MSG_SIZE 64

int flag;
void sighandler(int);

typedef struct my_msgbuf {
    long mtype;
    int msg_qid;
    char payload[MSG_SIZE]; 
} my_msgbuf;



bool not_contains(int val, int *arr, int size){
    int i;
    for (i=0; i < size; i++) {
        if (arr[i] == val)
            return false;
    }
    return true;
}

int send_message(int qid, my_msgbuf *qbuf)
{
    int res;
    int length;
    length = sizeof(my_msgbuf) - sizeof(long);        
    res = msgsnd(qid, qbuf, length, 0);
    if (res == -1) {
        perror("Msgsnd: ");
        return 1;
    }
    return 0;
}

int read_message(int qid, long type, my_msgbuf *qbuf)
{
    int res;
    int length;
    length = sizeof(my_msgbuf) - sizeof(long);   
    res = msgrcv(qid, qbuf, length, type, 0);
    if (res == -1) {
        perror("Msgrcv: ");
        return 1;
    }
    return 0;
}

int main (int argc, char* argv[]) {
    int key = ftok("./server", 'I');
    if (key == -1) {
        perror("Ftok: ");
        return 1;
    }
    int server_qid = msgget(key, 0666 | IPC_CREAT);
    if (server_qid == -1) {
        perror("Msgget: ");
        return 1;
    }

    printf("Server has started\n");

    struct my_msgbuf r_msg;
    struct my_msgbuf s_msg;

    int clients_number = 0;

    char buffer[MSG_SIZE];

    int creds[255];

    flag = 1;
    if (signal(SIGINT, sighandler) == SIG_ERR)
        printf("I can't catch SIGINT\n");

    while(flag == 1){
        memset(buffer, 0, sizeof buffer);
        read_message(server_qid, 1, &r_msg);
        strcpy(buffer, r_msg.payload);
        printf("Received: %s", buffer);
        
        if(!strcmp(buffer,"ping\n")) {
            creds[clients_number] = r_msg.msg_qid;
            clients_number++;
        }

        s_msg.msg_qid = 0;
        s_msg.mtype = r_msg.msg_qid;
        strcpy(s_msg.payload, buffer);

        for(int i=0; i < clients_number; i++) {
            if (creds[i] == r_msg.msg_qid) {
                send_message(server_qid, &s_msg);
            }
        }
    }

    if (msgctl(server_qid, IPC_RMID, NULL) == -1) {
        fprintf(stderr, "Message queue could not be deleted.\n");
        exit(EXIT_FAILURE);
    }

    printf("Message queue was deleted.\n");
    return 0;
}

void sighandler(int signum) {
    if (signal(SIGINT, SIG_DFL) == SIG_ERR)
        printf("I can't catch SIGINT\n");
    flag = 0;
}