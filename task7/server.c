#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/wait.h>
#include <string.h>
#include <errno.h>
#include <sys/msg.h>
#include <stdbool.h>

#define MSG_SIZE 64

typedef struct my_msgbuf {
    long mtype;
    int msg_qid;
    char payload[MSG_SIZE]; 
} my_msgbuf;



bool not_contains(int val, int *arr, int size){
    int i;
    for (i=0; i < size; i++) {
        if (arr[i] == val)
            return false;
    }
    return true;
}

int send_message(int qid, my_msgbuf *qbuf)
{
    int res;
    int length;
    length = sizeof(my_msgbuf) - sizeof(long);        
    res = msgsnd(qid, qbuf, length, 0);
    if (res == -1) {
        perror("Msgsnd: ");
        return 1;
    }
    return 0;
}

int read_message(int qid, long type, my_msgbuf *qbuf)
{
    int res;
    int length;
    length = sizeof(my_msgbuf) - sizeof(long);   
    res = msgrcv(qid, qbuf, length, type, 0);
    if (res == -1) {
        perror("Msgrcv: ");
        return 1;
    }
    return 0;
}

int main (int argc, char* argv[]) {
    int key = ftok("./server", 'I');
    if (key == -1) {
        perror("Ftok: ");
        return 1;
    }
    int server_qid = msgget(key, 0666 | IPC_CREAT);
    if (server_qid == -1) {
        perror("Msgget: ");
        return 1;
    }

    printf("Server has started\n");

    struct my_msgbuf r_msg;
    struct my_msgbuf s_msg;
    int client_qid[255];

    int clients_number = 0;

    char buffer[MSG_SIZE];

    while(1){
        memset(buffer, 0, sizeof buffer);
        read_message(server_qid, 0, &r_msg);
        strcpy(buffer, r_msg.payload);
        printf("Received: %s", buffer);

        bool cnt = not_contains(r_msg.msg_qid, client_qid, clients_number);
        if (cnt) {
            client_qid[clients_number] = r_msg.msg_qid;
            clients_number++;
        }

        for(int i=0; i < clients_number; i++) {
            s_msg.mtype = r_msg.mtype;
            s_msg.msg_qid = 0;
            strcpy(s_msg.payload, buffer);
            send_message(client_qid[i], &s_msg);
        }
    }
}
