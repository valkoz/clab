#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/wait.h>
#include <string.h>
#include <errno.h>
#include <sys/msg.h>

#define MSG_SIZE 64
//run server ./server
//run clients: ./client 1 1 ./client 2 1 ./client 3 2 ./client 4 2
// ./client 1 1 : argv[1] to generate key, argv[2] receive msg type
typedef struct my_msgbuf {
    long mtype;
    int msg_qid;
    char payload[MSG_SIZE]; 
} my_msgbuf;

int send_message(int qid, my_msgbuf *qbuf)
{
    int res;
    int length;
    length = sizeof(my_msgbuf) - sizeof(long);        
    res = msgsnd(qid, qbuf, length, 0);
    if (res == -1) {
        perror("Msgsnd: ");
        return 1;
    }
    return 0;
}

int read_message(int qid, long type, my_msgbuf *qbuf)
{
    int res;
    int length;
    length = sizeof(my_msgbuf) - sizeof(long);   
    res = msgrcv(qid, qbuf, length, type, 0);
    if (res == -1) {
        perror("Msgrcv: ");
        return 1;
    }
    return 0;
}

int main (int argc, char* argv[]) {
    int key = ftok("./server", 'I');
    if (key == -1) {
        perror("Ftok: ");
        return 1;
    }
    int server_qid = msgget(key, 0666);
    if (server_qid == -1) {
        perror("Msgget: ");
        return 1;
    }

    key = ftok("./client", atoi(argv[1]));
    if (key == -1) {
        perror("Ftok: ");
        return 1;
    }
    int client_qid = msgget(key, 0666 | IPC_CREAT);
    if (client_qid == -1) {
        perror("Msgget: ");
        return 1;
    }

    printf("Client has started\n");

    struct my_msgbuf r_msg;
    struct my_msgbuf s_msg;
    char buffer[MSG_SIZE];

    int pid;
    pid = fork();
    if (pid == 0) {
        s_msg.mtype = atoi(argv[2]);
        s_msg.msg_qid = client_qid;
        strcpy(s_msg.payload, "ping\n");
        send_message(server_qid, &s_msg);
        while(1) {
            memset(buffer, 0, sizeof buffer);
            read(0, buffer, MSG_SIZE);
            s_msg.mtype = atoi(argv[2]);
            s_msg.msg_qid = client_qid;
            strcpy(s_msg.payload, buffer);
            send_message(server_qid, &s_msg);
        }
    }
    else {
        while(1) {
            read_message(client_qid, atoi(argv[2]), &r_msg);
            printf("Response from server: %s", r_msg.payload);
        }
    }
}
